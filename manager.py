from connect import Connect

#geting connection
connection = Connect.connection()
#geting database throug connection
database = connection.get_database("password")
#geting the user collection from the database
users = database.users
#geting password connection from the database
sites = database.sites

def getString(inputMessage):
    """ get a nonumpty string """
    result = ""
    while result == "":
        result = str(input(inputMessage))
    return result

def getUserID(email):
    """ query the id of the user according to his email adress """
    listUser =  list(users.find({"email": email})) 

    #insert this user if he doesn't exist in the users collection
    if len(listUser) == 0:
        password = getString("you doesn't exit in the database, enter a new password:\n")
        users.insert_one({"email": email, "password": password})
    #
    else:
        password = getString("enter your password to login in the database\n")
        listUser =  list(users.find({"email": email, "password": password}))
        if len(listUser) == 0:
            return None
        else:
            return listUser[0]['_id']
    return (list(users.find({"email": email}))[0])['_id']

def insertWebsite(userId):
    """ insert new information for the userId """
    website = getString("\ninsert the name of the website\n")
    login = getString(f"\ninsert your {website} login\n")
    password = getString("\ninsert your password\n")
    sites.insert_one({"userId": userId, "website": website, "login": login, "password": password})
    print("saved\n")

def getLoginPassword(userId, website):
    """ get the login and password of a user """
    result = list(sites.find({'userId': userId, 'website': website}))
    if len(result) == 0:
        return ()
    else:
        return result[0]['login'], result[0]['password']

def getList(userId):
    """ get the list of website """
    return [info['website'] for info in list(sites.find({'userId': userId}))]

def deleteWebsite(userId, website):
    """ delete a website from user's list """
    sites.delete_one({'userId': userId, 'website': website})
    print("deleted\n")

def updatePasssword(userId, website, newPassword):
    """ update a website's password """
    sites.update_one({'userId': userId, 'website': website}, {'$set': {'password': newPassword}})
    print("updated\n")

if __name__ == "__main__":
    userId = None
    while userId == None:
        email = str(input("Enter your email: "))
        userId = getUserID(email)
    action = ["i", "r", "d", "u", "q", "l"] #insert, research, delete, update, quit, list
    commande = ""
    print("*"*12 +" Welcom to password manager " + "*"*12 + "\n")

    while commande != "q":
        print("-"*20+"\n")
        commande = getString("tape:  i (insert), r (research), d (delete), u (update), q (quit), l (list)\n")
        if commande not in action:
            print("unkown commande")
        elif commande == "i": 
            #insert information about a new website
            insertWebsite(userId)
        elif commande == "r":
            #research for the password of a specific website
            website = getString("\ninsert the name of the website you are loking for\n")
            results = getLoginPassword(userId, website)
            if len(results)==0:
                print("website not found :/\n")
            else:
                login, password = results
                print(f"------>  login: {login}   password: {password}\n")
        elif commande == "l":
            #show the list of website of the current user
            myList = getList(userId)
            if len(myList) == 0:
                print("there is not website in your list\n")
            else:
                for website in myList:
                    print(website)
        elif commande == "d":
            #delete a specific website
            website = getString("\ninsert the name of the website that you want to delete\n")
            deleteWebsite(userId, website)
        elif commande == "u":
            #update the password of a specif website
            website = getString("\ninsert the name of the website that you want to update\n")
            newPassword = getString(f'\ninsert the new password for {website}\n')
            updatePasssword(userId, website, newPassword)
        

