#Password manager
---
**Version 1.0.0**

We all have a lot of login and password on the internet, which makes it impossible to remember.
That's why you should use a password manager.

This one is written in python, and stores all of your information in mongoDB atlas.

you can:

- sign in (email, password)
- acces the list of websites.
- research the login and password for a specific website
- update the password for a specific website
- delete a website

**Compilation**

First activate the virtual environement (On Unix/Mac)

` source password-env/bin/activate`

Then run the program 

` python3 manager.py  `

To test the program use:

email: `test@gmail.com`

password: `test`

---

## License & copyright
© Sarobidy RAPETERAMANA

# 
